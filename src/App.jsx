import React from 'react';
import Layout from './layout/Layout.component';
import './assets/styles/styles.min.css';

const App = () => (
  <Layout />
);

export default App;
