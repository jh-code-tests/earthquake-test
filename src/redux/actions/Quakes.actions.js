import axios from 'axios';

const config = {
  headers: {
    accept: 'application/json',
  },
};

// Returns quake data from the api.
export const getQuakes = async (params) => {
  const url = `${process.env.REACT_APP_API_URL}/fdsnws/event/1/query`;
  config.params = params;

  const res = await axios.get(url, config);
  return res;
};

export default getQuakes;
