import { LOAD_QUAKES } from '../ActionTypes';
import { getQuakes } from '../actions/Quakes.actions';
import convertQuakeData from '../../assets/utilities/dataConversion';

export const loadQuakes = filter => async (dispatch) => {
  const time = new Date();
  // sets the time to be minus however many hours the filter is set to.
  time.setHours(time.getHours() - parseInt(filter, 10));

  // The query parameters
  const params = {
    format: 'geojson',
    limit: 10,
    orderby: 'magnitude',
    starttime: time,
  };

  const quakeData = await getQuakes(params);
  // Converts the returned data to a usable format.
  const formattedData = convertQuakeData(quakeData.data.features);

  return dispatch({
    type: LOAD_QUAKES,
    data: formattedData,
  });
};

export default loadQuakes;
