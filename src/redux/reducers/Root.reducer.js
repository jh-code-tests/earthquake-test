import { combineReducers } from 'redux';
import Quakes from './Quakes.reducer';

const rootReducer = () => combineReducers({
  Quakes,
});

export default rootReducer;
