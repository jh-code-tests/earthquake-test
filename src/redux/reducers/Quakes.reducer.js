import { LOAD_QUAKES } from '../ActionTypes';

const initialState = {
  data: [],
};

const Quakes = (state = initialState, action) => {
  switch (action.type) {
    // Loads the quake data into the state
    case LOAD_QUAKES:
      return {
        ...state,
        data: action.data,
      };
    default:
      return state;
  }
};

export default Quakes;
