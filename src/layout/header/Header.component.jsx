import React from 'react';

const Header = () => (
  <div id="Header">
    <div id="Title">
      <h2>Web Developer Test</h2>
    </div>
    <div id="Credit">
      <h4>Created by Jack Hayes</h4>
    </div>
  </div>
);

export default Header;
