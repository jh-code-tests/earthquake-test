import React from 'react';
import Header from './header/Header.component';
import Body from './body/Body.component';

const Layout = () => (
  <div id="Layout">
    <Header />
    <Body />
  </div>
);

export default Layout;
