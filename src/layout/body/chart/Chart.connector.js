import { connect } from 'react-redux';
import Chart from './Chart.component';

const mapStateToProps = state => ({
  data: state.Quakes.data,
});

export default connect(
  mapStateToProps,
)(Chart);
