import React from 'react';
import PropTypes from 'prop-types';
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
} from 'recharts';

import CustomTooltip from './custom_tooltip/CustomTooltip.component';

const Chart = ({ data }) => {
  if (data.length !== 0) {
    return (
      <div id="ChartContainer">
        <div id="ChartsWrapper">
          <ResponsiveContainer>
            <LineChart data={data}>
              <Line type="monotone" dataKey="mag" stroke="#8884d8" />
              <CartesianGrid stroke="#ccc" />
              <XAxis dataKey="time" />
              <YAxis />
              <Tooltip content={<CustomTooltip />} />
            </LineChart>
          </ResponsiveContainer>
        </div>
        <div id="ChartInfo">
          <p>Hover over each point for more information.</p>
        </div>
      </div>
    );
  }
  return null;
};

Chart.propTypes = {
  data: PropTypes.array.isRequired,
};

export default Chart;
