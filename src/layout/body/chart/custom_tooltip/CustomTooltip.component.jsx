import React from 'react';
import PropTypes from 'prop-types';

const CustomTooltip = ({ payload }) => {
  if (payload.length !== 0) {
    return (
      <div className="custom-tooltip">
        <p>
          Time:&nbsp;
          {payload[0].payload.time}
        </p>
        <p>
          Date:&nbsp;
          {payload[0].payload.date}
        </p>
        <p>
          Location:&nbsp;
          {payload[0].payload.location}
        </p>
        <p>
          Coords:&nbsp;
          {payload[0].payload.coordinates}
        </p>
      </div>
    );
  }
  return null;
};

CustomTooltip.defaultProps = {
  payload: null,
};

CustomTooltip.propTypes = {
  payload: PropTypes.array,
};

export default CustomTooltip;
