import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Filter = ({ loadQuakes }) => {
  const [filterVal, setFilterVal] = useState('12');

  return (
    <div id="FilterContainer" className="body-item">
      <div id="FilterLabel">
        Displaying earthquake data from the last:
      </div>
      <select
        name="filter"
        id="Filter"
        value={filterVal}
        onChange={(e) => {
          setFilterVal(e.target.value);
          loadQuakes(e.target.value);
        }}
      >
        <option value="12">12 hours</option>
        <option value="24">24 hours</option>
      </select>
    </div>
  );
};

Filter.propTypes = {
  loadQuakes: PropTypes.func.isRequired,
};

export default Filter;
