import { connect } from 'react-redux';
import Filter from './Filter.component';

import { loadQuakes } from '../../../redux/action_creators/Quakes.actioncreator';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  loadQuakes: filter => dispatch(loadQuakes(filter)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Filter);
