import React from 'react';
import Filter from './filter/Filter.connector';
import Chart from './chart/Chart.connector';

const Body = () => (
  <div id="Body">
    <Filter />
    <Chart />
  </div>
);

export default Body;
