// Converts returned data from api into usable data
import moment from 'moment';

const convertQuakeData = (data) => {
  const formattedData = [];
  let tmp;

  // Orders data by time.
  const orderedData = data.sort((a, b) => (a.properties.time > b.properties.time ? 1 : -1));

  orderedData.forEach((i) => {
    tmp = {};
    tmp.time = moment(i.properties.time).format('LT'); // formats to H:MM
    tmp.mag = i.properties.mag;
    tmp.date = moment(i.properties.time).format('LL'); // Formats to M DD, YYYY
    tmp.coordinates = `x: ${i.geometry.coordinates[0]}, y: ${i.geometry.coordinates[1]}`;
    tmp.location = i.properties.place;
    formattedData.push(tmp);
  });

  return formattedData;
};

export default convertQuakeData;
