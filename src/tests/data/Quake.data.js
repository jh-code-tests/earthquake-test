const beforeFormatData = [
  {
    geometry: {
      coordinates: [1, 1],
    },
    properties: {
      mag: 1,
      time: 1561154598795,
      place: 'test 1',
    },
  },
];

const afterFormatData = [
  {
    coordinates: 'x: 1, y: 1', date: 'June 22, 2019', location: 'test 1', mag: 1, time: '8:03 AM',
  },
];

export { beforeFormatData, afterFormatData };
