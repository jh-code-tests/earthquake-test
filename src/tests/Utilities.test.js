import convertQuakeData from '../assets/utilities/dataConversion';

import { beforeFormatData, afterFormatData } from './data/Quake.data';

/**
 * Test Sucessful Verified login
 *
 */
test('should sucessfully load quake data', async () => {
  const formattedData = convertQuakeData(beforeFormatData);

  expect(formattedData).toEqual(afterFormatData);
});
